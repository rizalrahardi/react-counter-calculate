import { Component } from "react"
import { Button } from "reactstrap"

class Counter extends Component {
  constructor() {
    super();
    this.state = {
      number: 0
    }

    this.handlePlus = this.handlePlus.bind(this)
    this.handleMinus = this.handleMinus.bind(this)
    this.handleReset = this.handleReset.bind(this)
  }

  handlePlus() {
    this.setState({
      number: this.state.number + 1
    })
  }

  handleMinus() {
    this.setState({
      number: this.state.number - 1
    })
  }

  handleReset() {
    this.setState({
      number: 0
    })
  }

  render() {
    return (
      <div className="row mt-5">
        <div className="col-12">
          <h1>{this.props.title}</h1>
          <h2 className="text-center">{this.state.number}</h2>
          <div className="text-center">
            <Button
              color="success"
              size="lg"
              className="mr-2"
              onClick={this.handlePlus}>
                Plus
            </Button>
            <Button
              color="danger"
              size="lg"
              className="mr-2"
              onClick={this.handleReset}>
                Reset
            </Button>
            <Button
              color="warning"
              size="lg"
              className="mr-2"
              onClick={this.handleMinus}>
                Minus
            </Button>
          </div>
        </div>
      </div>
    )
  }
}

export default Counter;