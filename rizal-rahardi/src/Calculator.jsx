import { useState } from "react"
import { Button, Col, Form, FormGroup, Input, Row } from "reactstrap";

const Calculator = (props) => {

  const initialValue = {
    x: 0,
    y: 0,
    operator: "tambah"
  }

    const [total, setTotal] = useState(0)
    const [values, setValues] = useState(initialValue)

    const handleChange = (e) =>{
      const { name, value} = e.target
      setValues({
        ...values,
        [name]: value
      })
    }

    const handleCalculate = () => {
      const {x, y, operator} = values
      if(operator === "tambah") {
        setTotal(parseInt(x)+parseInt(y))
      } else if (operator === "kurang") {
        setTotal(parseInt(x)-parseInt(y))
      } else if (operator === "kali") {
        setTotal(parseInt(x)*parseInt(y))
      } else if (operator === "bagi") {
        setTotal(parseInt(x)/parseInt(y))
      }
    }

    return(
        <div className="row mt-5">
        <div className="col-12">
          <h1>{props.title}</h1>
          <h2 className="text-center">{total}</h2>
          <Form>
            <Row form>
              <Col md={5}>
                <FormGroup>
                  <Input name="x" type="number" placeholder="X value..." onChange={handleChange} />
                </FormGroup>
              </Col>
              <Col md={2}>
                <FormGroup>
                  <Input type="select" name="operator" onChange={handleChange}>
                    <option value="tambah">+</option>
                    <option value="kurang">-</option>
                    <option value="kali">*</option>
                    <option value="bagi">/</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col md={5}>
                <FormGroup>
                  <Input name="y" type="number" placeholder="Y value..." onChange={handleChange} />
                </FormGroup>
              </Col>
            </Row>
            <div className="text-center">
              <Button color="success" size="lg" onClick={handleCalculate}>Calculate</Button>
            </div>
          </Form>
        </div>
      </div>
    )
}

export default Calculator;