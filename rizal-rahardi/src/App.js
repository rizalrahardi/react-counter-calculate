import Counter from "./Counter"
import Calculator from "./Calculator"
import Calculate from "./Calculate"

const App = () => {
  return (
    <div className="container">
      <Counter title="React Counter" />
      <Calculator title="React Hooks Calculator" />
      <Calculate title="React Non-Hooks Calculator" />

    </div>
  );
}

export default App;